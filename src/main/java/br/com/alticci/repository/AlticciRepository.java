package br.com.alticci.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;

import br.com.alticci.entity.AlticciEntity;

@Repository
public interface AlticciRepository extends JpaRepository<AlticciEntity, Integer>{
	Optional<AlticciEntity> findByPosition(Long position);
	AlticciEntity findAlticciEntityByPosition(Long position);
}
