package br.com.alticci.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class AlticciEntity {
	
	@Id
    @GeneratedValue
	private Integer id;
	private Long position; 
	private Double value;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public Long getPosition() {
		return position;
	}
	public void setPosition(Long position) {
		this.position = position;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}

	public AlticciEntity(Long position, Double value) {
		super();
		this.position = position;
		this.value = value;
	} 
	public AlticciEntity() {
		super();
	} 

}
