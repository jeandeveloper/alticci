package br.com.alticci.controller;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.alticci.service.AlticciService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/alticci")
public class AlticciController {
	
	private final AlticciService alticciService;
	
	@GetMapping(value = "/{n}")
	 @ApiOperation(value = "Finds alticci sequency",
	    notes = "Cache alticci sequency",
	    responseContainer = "List")
	@Cacheable("alticciEntity")
	public ResponseEntity<Double> getSequence(@PathVariable Long n){
		
		return ResponseEntity.ok(alticciService.getSequenciaAlticci(n));
	}

	public AlticciController(AlticciService alticciService) {
		super();
		this.alticciService = alticciService;
	}
	
	
}
