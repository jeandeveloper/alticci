package br.com.alticci.service;

import org.springframework.stereotype.Service;

import com.google.common.base.Optional;

import br.com.alticci.entity.AlticciEntity;
import br.com.alticci.repository.AlticciRepository;

@Service
public class AlticciService {
	
	private final AlticciRepository alticciRepository;
	
	public Double getSequenciaAlticci(Long n) {
		inicializarSequencia();
		Optional<AlticciEntity> ent = alticciRepository.findByPosition(n);
		if (ent.isPresent()) {
			return ent.get().getValue();
		}else {
			findPosition(n);
			return alticciRepository.findByPosition(n).get().getValue();
		}
	}
	
	public AlticciEntity findPosition(Long n) {
		
		if (n < 3) {
			return  alticciRepository.findByPosition(n).get();
		}else if(alticciRepository.findByPosition(n).isPresent()) {
			return alticciRepository.findByPosition(n).get();
		}
		Optional<AlticciEntity> parametro1 = alticciRepository.findByPosition(n-3);
		Optional<AlticciEntity> parametro2 = alticciRepository.findByPosition(n-2);
		
		if (!parametro1.isPresent() || !parametro2.isPresent()) {
			parametro1  = Optional.of(findPosition(n-3));
			parametro2  = Optional.of(findPosition(n-2));
			System.out.println("numero salvo " + n );
			return alticciRepository.save(new AlticciEntity(n, parametro1.get().getValue()+parametro2.get().getValue()));
		}else {
			System.out.println("numero salvo " + n );
			return alticciRepository.save(new AlticciEntity(n, parametro1.get().getValue()+parametro2.get().getValue()));
		}
		 
	}
	
	public void inicializarSequencia() {
		Optional<AlticciEntity> ent = alticciRepository.findByPosition(0l);
		if (!ent.isPresent()) {
			alticciRepository.save(new AlticciEntity(0l, 0d));
			alticciRepository.save(new AlticciEntity(1l, 1d));
			alticciRepository.save(new AlticciEntity(2l, 1d));
		}
	}

	public AlticciService(AlticciRepository alticciRepository) {
		super();
		this.alticciRepository = alticciRepository;
	}
	
}
